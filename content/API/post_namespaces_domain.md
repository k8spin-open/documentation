+++
title = "/namespaces/{{namespace_name}}/domains/add"
chapter = false
weight = 6
pre = "<b>POST </b>"
+++

### Add an allowed domain in a namespace

In this endpoint you can add an allowed domain to a namespace. Part of the custom-domains feature.

**URL**

`/namespaces/{{namespace_name}}/domains/add`

**Method**

`POST`

**URL Params**

You have to send as path parameter the name of the namespace:

- *`namespace_name`*: **[REQUIRED]** Short name of the namespace.

**Data Params**

You must send a body in json containing the domain name *(wildcard are also supported)* you want to allow in the namespace. The attributes of the json object are:

- *`domain`*: **[REQUIRED]** Domain name or subdomain or wildcard of the custom domain you want to allow in the namespace.

**Success Response**

*Code:* 200

*Content:* ` `

**Error Responses**

- If you don't send the mandatory attribute:

    **Code**: 400 BAD REQUEST

    **Content**: `{ "error" : "No namespace_name provided" }`

- If you send an invalid domain:

    **Code**: 409 CONFLICT

    **Content**: `{ "error" : "Invalid domain" }`

**Sample Calls**

```bash
$ curl -H "Authorization: Bearer SAMPLE-TOKEN" -H "Content-Type: application/json" --request POST --data '{"domain":"example.com"}' https://api.k8spin.cloud/namespaces/my_own_namespace/domains/add

$ curl -H "Authorization: Bearer SAMPLE-TOKEN" -H "Content-Type: application/json" --request POST --data '{"domain":"k8spin.cloud"}' https://api.k8spin.cloud/namespaces/my_own_namespace/domains/add
{
  "error": "Invalid domain"
}
```