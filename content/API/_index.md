+++
title = "API"
chapter = false
weight = 4
pre = "<b>4. </b>"
+++

### Let's go!!!

We release version 0 of our API. Very simple but **powerful**.

#### Common

We use token-based authn & authz. This must be sent in the form of a [Bearer Authorization](https://swagger.io/docs/specification/authentication/bearer-authentication/) header:
```
Authorization: Bearer <token>
```

You can find your token on the [console](https://console.k8spin.cloud) *(log in first, look under your profile picture)*.

The API base is: `/`.
Current API url: [https://api.k8spin.cloud](https://api.k8spin.cloud)

##### Error responses

The following responses are returned in all endpoints in the following cases:

- If you do not send token in the header `Authorization`

    **Code**: 401 UNAUTHORIZED

    **Content**: `{ "error" : "No token provided. See our docs at docs.k8spin.cloud" }`

- If the token is invalid:

    **Code**: 401 UNAUTHORIZED

    **Content**: `{ "error" : "Invalid token" }`
