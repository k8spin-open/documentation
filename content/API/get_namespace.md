+++
title = "/namespaces/{NAME}"
chapter = false
weight = 3
pre = "<b>GET </b>"
+++

### Get namespace credentials

With this endpoint you will be able to download the access credentials to the namespace.

**URL**

`/namespaces/{NAME}`

**Method**

`GET`

**URL Params**

You have to send as path parameter the name of the namespace:

- *`NAME`*: **[REQUIRED]** Short name of the namespace.

**Success Response**

*Code:* 200

*Content:*
```
apiVersion: v1
kind: Config

clusters:
- cluster:
    certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSURERENDQWZTZ0F3SUJBZ0lSQU9PbXZkRHlBL0pJTWI5YVJCTFJ6Mjh3RFFZSktvWklodmNOQVFFTEJRQXcKTHpFdE1Dc0dBMVVFQXhNa1lXUmpabVl6TVRRdE9HSmlZeTAwT1RJMUxUZzNZMlV0T1dJek1EQTJNekppWVdVMQpNQjRYRFRFNU1EUXhNekEzTWpFeU5sb1hEVEkwTURReE1UQTRNakV5Tmxvd0x6RXRNQ3NHQTFVRUF4TWtZV1JqClptWXpNVFF0T0dKaVl5MDBPVEkxTFRnM1kyVXRPV0l6TURBMk16SmlZV1UxTUlJQklqQU5CZ2txaGtpRzl3MEIKQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBeGJ2SXBzZXIwandVaWtBaFJ5ZVpUbXBYYlpMajBFWTJ4L0pIVDQ1VQowVytEaVRrWE1sai8rRGdPOTRyeHNBcEVicUdCNTJ4cDl3WTlINUt2R3AxL2lHRDQrTnZnTmVzUnRRTjNxYVJBClEzNFkzVGJ1cFk3ckFWTlMzcFdaS2dYeWE0VzZHVVcxK0l3SWdsRU94MVp3RkNkQlJERjc0ODYwTFlSNlVlVW8KTUhiNE9VT1EwR1pVcC9ibTU3RDVuZDk0eHExTHgzQmd3aDNnbWZLU3dKUVhEYlBTZ1JyQWFMcUVxOUZ4R0s5VgpNOG9jckhSeG9sNWpvOHdqK240a1NPK2VvT2lJcmUvVFo4YjhjMmhzR0kxZ1FvcFNta0VKUlkyZGk2Vjd6VlFVCnJqSnU4Z3VhQ3NKUGtHQldCSUNYM2V3cnRyaUF5YS96dlVoRC9NTW95QVg1UndJREFRQUJveU13SVRBT0JnTlYKSFE4QkFmOEVCQU1DQWdRd0R3WURWUjBUQVFIL0JBVXdBd0VCL3pBTkJna3Foa2lHOXcwQkFRc0ZBQU9DQVFFQQp2SHBsK3ByQWt3QWN3bFNtSmxwOUt6NTdVc3Y1akRGYktJVDRKaXZOMkowVEkrWlBaR2ZjaVZLUXFEL2pZMkdiCm5sb0RSaU5FRzIxWUtiS085OE8vVWY2Nndoek5ER1dzRGdsOE8yMmo0WGZLTkRJY0w5RFhQVDBDVVM5UmVLTkkKMkNEaitLNFVRQlNuWndkWEpIcEVTbDRqZTR1Rjg5QWtYLytpR2cycEVOSlQ0dFh0ODRMb3ZoLzZmdHF2Y0lxMgpXa0ZkdE4xK2daSkFwTXZ0dmNjZ3J1Q2prQlJkK2xNVE1wTW8vdlVSeW9TQy8wZHdmZFFWTzNJbnRTUlU5L3RYCkFIRXBNdzNlUlhwN2FmK2pyMlM4VkZwN0hGdnlJL2NyY0ExMzhsYTV1S01UbGxOYVAxeXJPblZxTnVWT0c2eUQKSWk1anpranR4YlVqUkI1RGs2QlFpdz09Ci0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0K
    server: https://35.198.68.19
  name: angelbarrerasanchez-gmail-com-xyz

users:
- name: angelbarrerasanchez-gmail-com-xyz
  user:
    token: SECRET

contexts:
- context:
    cluster: angelbarrerasanchez-gmail-com-xyz
    namespace: angelbarrerasanchez-gmail-com-xyz
    user: angelbarrerasanchez-gmail-com-xyz
  name: angelbarrerasanchez-gmail-com-xyz

current-context: angelbarrerasanchez-gmail-com-xyz
```

**Error Responses**

- If you query for a namespace name that doesn't exist:

    **Code**: 404 NOT FOUND

    **Content**: `{ "error" : "The namespace does not exists" }`


**Sample Call**

```bash
$ curl -s -H "Authorization: Bearer SAMPLE-TOKEN" "https://api.k8spin.cloud/namespaces/xyz" > k8spin.config
$ export KUBECONFIG=$(pwd)/k8spin.config
$ kubectl get issuer
NAME                            AGE
angelbarrerasanchez-gmail-com   23m
[]

$ curl  -H "Authorization: Bearer SAMPLE-TOKEN" "https://api.k8spin.cloud/namespaces/xyz-2"
{
  "error": "The namespace does not exists"
}
```
