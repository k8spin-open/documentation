+++
title = "/namespaces/{{namespace_name}}/domains"
chapter = false
weight = 5
pre = "<b>GET </b>"
+++

### List allowed domains in a namespace

In this endpoint you can discover the allowed domains in the namespace. Part of the custom-domains feature.

**URL**

`/namespaces/{{namespace_name}}/domains`

**Method**

`GET`

**URL Params**

You have to send as path parameter the name of the namespace:

- *`namespace_name`*: **[REQUIRED]** Short name of the namespace.


**Success Response**

*Code:* 200

*Content:* `["*.my-user.apps.k8spin.cloud"]`

**Error Responses**

- If you don't send the mandatory attribute:

    **Code**: 400 BAD REQUEST

    **Content**: `{ "error" : "No namespace_name provided" }`

**Sample Calls**

```bash
$ curl -H "Authorization: Bearer SAMPLE-TOKEN" https://api.k8spin.cloud/namespaces/my_own_namespace/domains
[
  "*.my-user.apps.k8spin.cloud"
]


$ curl -H "Authorization: Bearer SAMPLE-TOKEN" https://api.k8spin.cloud/namespaces/invalid_namespace/domains
{
  "reason": "The namespace does not exists",
  "status": 404
}
```