+++
title = "/namespaces/{NAME}"
chapter = false
weight = 4
pre = "<b>DELETE </b>"
+++

### Delete namespace

Using this endpoint you can delete a namespace

**URL**

`/namespaces/{NAME}`

**Method**

`DELETE`

**URL Params**

You have to send as path parameter the name of the namespace:

- *`NAME`*: **[REQUIRED]** Short name of the namespace.

**Success Response**

*Code:* 202

*Content:* ` `

**Error Responses**

- If you query for a namespace name that doesn't exist:

    **Code**: 404 NOT FOUND

    **Content**: `{ "error" : "The namespace does not exists" }`


**Sample Call**

```bash
$ curl -X DELETE -H "Authorization: Bearer SAMPLE-TOKEN" "https://api.k8spin.cloud/namespaces/xyz"
# After a few seconds (deletion is async)
$ curl -H "Authorization: Bearer SAMPLE-TOKEN" "https://api.k8spin.cloud/namespaces/xyz"
{
  "error": "The namespace does not exists"
}
```
