+++
title = "/namespaces"
chapter = false
weight = 2
pre = "<b>POST </b>"
+++

### Create namespace

In this endpoint you can create a namespace

**URL**

`/namespaces`

**Method**

`POST`

**Data Params**

You must send a body in json containing the name of the namespace you want to create. The attributes of the json object are:

- *`namespace_name`*: **[REQUIRED]** Short name of the namespace.
- *`resources`*: **[REQUIRED]** Resource object. Determines the size and price of a namespace.
- *`resources.cpu`*: **[REQUIRED]** CPU requested in milicores.
- *`resources.mem`*: **[REQUIRED]** Memory *(RAM)* requested in megabytes.
- *`disks_size`*: **[REQUIRED]** Max storage requested in megabytes across all persistent volumes.

**Success Response**

*Code:* 201

*Content:* ` `

**Error Responses**

- If you reach the limit of allowed namespaces:

    **Code**: 401 UNAUTHORIZED

    **Content**: `{ "error" : "You can not create more namespace. You can create up to 1 namespaces" }`

- If you don't send the mandatory attribute:

    **Code**: 400 BAD REQUEST

    **Content**: `{ "error" : "No namespace_name provided" }`

- If you try to create a namespace with an already used name:

    **Code**: 409 CONFLICT

    **Content**: `{ "error" : "Namespace already exists. Choose a different name" }`


**Sample Calls**

```bash
$ curl -H "Authorization: Bearer SAMPLE-TOKEN" -H "Content-Type: application/json" --request POST --data '{"namespace_name":"xyz","resources":{"cpu":100,"mem":128,"disks_size":0}}' https://api.k8spin.cloud/namespaces

$ curl -H "Authorization: Bearer SAMPLE-TOKEN" -H "Content-Type: application/json" --request POST --data '{"namespace_name":"xyz-2","resources":{"cpu":100,"mem":128,"disks_size":0}}' https://api.k8spin.cloud/namespaces
{
  "error": "You can not create more namespace. You can create up to 1 namespaces"
}
```