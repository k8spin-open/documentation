+++
title = "/namespaces"
chapter = false
weight = 1
pre = "<b>GET </b>"
+++

### List namespaces

In this endpoint you will be able to see all your namespace created in your account as a list.

**URL**

`/namespaces`

**Method**

`GET`

**Success Response**

*Code:* 200

*Content:*
```
[
  {
    "expiration": "2019-06-07T17:36:18Z",
    "ingress_whitelist": [
      "*.angelbarrerasanchez.apps.k8spin.cloud"
    ],
    "namespace": "angelbarrerasanchez-gmail-com-xyz",
    "namespace_name": "xyz",
    "resource_quotas": {
      "limits": {
        "count/configmaps": "10",
        "count/cronjobs.batch": "10",
        "count/deployments.apps": "10",
        "count/deployments.extensions": "10",
        "count/jobs.batch": "10",
        "count/pods": "10",
        "count/replicasets.apps": "10",
        "count/replicationcontrollers": "10",
        "count/secrets": "10",
        "count/services": "10",
        "count/statefulsets.apps": "10",
        "limits.cpu": "125m",
        "limits.memory": "400Mi",
        "persistentvolumeclaims": "1",
        "regional.storageclass.storage.k8s.io/persistentvolumeclaims": "0",
        "regional.storageclass.storage.k8s.io/requests.storage": "0",
        "requests.cpu": "125m",
        "requests.memory": "400Mi",
        "requests.storage": "100Mi",
        "services.loadbalancers": "0",
        "services.nodeports": "0",
        "standard.storageclass.storage.k8s.io/persistentvolumeclaims": "1",
        "standard.storageclass.storage.k8s.io/requests.storage": "100Mi"
      },
      "status": {
        "count/configmaps": "0",
        "count/cronjobs.batch": "0",
        "count/deployments.apps": "0",
        "count/deployments.extensions": "0",
        "count/jobs.batch": "0",
        "count/pods": "0",
        "count/replicasets.apps": "0",
        "count/replicationcontrollers": "0",
        "count/secrets": "3",
        "count/services": "0",
        "count/statefulsets.apps": "0",
        "limits.cpu": "0",
        "limits.memory": "0",
        "persistentvolumeclaims": "0",
        "regional.storageclass.storage.k8s.io/persistentvolumeclaims": "0",
        "regional.storageclass.storage.k8s.io/requests.storage": "0",
        "requests.cpu": "0",
        "requests.memory": "0",
        "requests.storage": "0",
        "services.loadbalancers": "0",
        "services.nodeports": "0",
        "standard.storageclass.storage.k8s.io/persistentvolumeclaims": "0",
        "standard.storageclass.storage.k8s.io/requests.storage": "0"
      }
    },
    "status": "Active"
  }
]
```

**Sample Call**

```bash
$ curl -H "Authorization: Bearer SAMPLE-TOKEN" "https://api.k8spin.cloud/namespaces"
[]
```