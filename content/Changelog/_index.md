+++
title = "Changelog"
chapter = true
weight = 7
pre = "<b>7. </b>"
+++

### 4 Jan 2020

- [cert-manager](https://cert-manager.io/docs/) Updated. From 0.10.0 to 0.12.0. [Check breaking changes here](https://cert-manager.io/docs/installation/upgrading/upgrading-0.10-0.11/#additional-annotation-changes)
    - Updated [public documentation](https://gitlab.com/k8spin-open/documentation) site.
    - Updated the [hello-world](https://gitlab.com/k8spin-open/examples/hello-world) example.

### 25 Jul 2019

- Leaving beta, production is here **Yes!!!!!!!**
- **Root containers** are permitted.
- **Landing page:** Improved!
- **New console:** New console, new experience.
- **Free tier:** 1 Namespace, 100m of CPU and 128Mi of RAM with no time restriction.
- **Unlocked tier:** 10 Namespaces, 1 CPU Core, 2048Mi of RAM pay per minutes. Soft limits, talk to us if you need anything else.
- **K8Spin CLI:** Easier k8spin experience. You can download latest CLI from [Github](https://github.com/k8spin/k8spin_cli)
- **Kubernetes Upgrade:** New version: 1.13.7

### 14 Jun 2019

- [gVisor](https://gvisor.dev/) applyed by default in all workloads. Checkout [documentation of the architecture](../architecture/#gvisor) to get more information.

### 1 Jun 2019

- Released public K8Spin API. Docs @ [https://docs.k8spin.cloud/api](../api)

### 18 May 2019

- Upgrade Kubernetes version from 1.12 to 1.13

### 28 Abr 2019

- [gVisor](https://gvisor.dev/) has been deployed as optional container runtime.

### 17 Abr 2019

- Two new permissions are added to all users
    - `get` Namespace. Required by helm tiller.
    - `list` StorageClass. Makes it easy to create persistent volumes.

### 30 Mar 2019

- Upgrade Kubernetes version from 1.12.5 to 1.12.6

### 7 Mar 2019

- Limited execution of containers using `root` as user. Motivated by: [Runc and CVE-2019-5736](https://kubernetes.io/blog/2019/02/11/runc-and-cve-2019-5736/)

### 4 Mar 2019

- Upgrade Kubernetes version from 1.11 to 1.12

### 21 Feb 2019
 
- Improve OPA deployment
    - Set rolling update strategy
    - POD antiaffinity
- LimitRange configurable by cluster (v1.6.2)