+++
title = "Getting started"
chapter = false
weight = 1
pre = "<b>1. </b>"
+++

## Let's do it

In this step-by-step guide we are going to deploy a web application exposing it to the whole world.

### Kubernetes client configuration

Assuming you have already downloaded your namespace configuration file *(let's name it `kubernetes.config`)* and you have the kubernetes client [*(kubectl)*](https://kubernetes.io/docs/tasks/tools/install-kubectl/) installed:

```bash
$ cd /tmp
$ ls kubernetes.config
kubernetes.config
# Lets configure the kubernetes client with the kubernetes.config config file
$ export KUBECONFIG=$(pwd)/kubernetes.config
$ kubectl get issuer
NAME                            AGE
angelbarrerasanchez-gmail-com   2m
```

With the creation of a namespace you already have available your [issuer](https://cert-manager.io/docs/concepts/issuer/) *(user)* to request for HTTPS TLS certificates, so easy.

### Prepare the deployment

We're going to deploy the [hello-world](https://gitlab.com/k8spin-open/examples/hello-world) example open to the whole world. Contains three different kubernetes objects: [Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/), [Service](https://kubernetes.io/docs/concepts/services-networking/service/) and [Ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/).

You can find more examples in our [examples Gitlab group](https://gitlab.com/k8spin-open/examples).

```bash
$ pwd
/tmp
# Clone the hello-world example
$ git clone https://gitlab.com/k8spin-open/examples/hello-world.git
Cloning into 'hello-world'...
remote: Enumerating objects: 8, done.
remote: Counting objects: 100% (8/8), done.
remote: Compressing objects: 100% (6/6), done.
remote: Total 8 (delta 1), reused 0 (delta 0)
Unpacking objects: 100% (8/8), done.
$ cd hello-world/
$ ls *.yml
deployment.yml  ingress.yml  service.yml
```

To make this example work, we must modify **two attributes** of the ingress definition:

Find `cert-manager.io/issuer: example-issuer` and replace the value *(`example-issuer`)* with your issuer name:

```bash
$ kubectl get issuer
NAME                            AGE
angelbarrerasanchez-gmail-com   37m
```

Result: `cert-manager.io/issuer: angelbarrerasanchez-gmail-com`.

Find `example-host.k8spin.cloud` *(there are two)* and replace with a subdomain within the subdomain assigned as whitelist.

Result: `hello-world.angelbarrerasanchez.apps.dev.k8spin.cloud`.
*Assuming your assigned subdomain whitelist is: `*.angelbarrerasanchez.apps.dev.k8spin.cloud`*.

Your ingress should looks like:

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: hello-world
  annotations:
    ingress.kubernetes.io/ssl-redirect: "true"
    cert-manager.io/issuer: angelbarrerasanchez-gmail-com
spec:
  tls:
  - hosts:
    - hello-world.angelbarrerasanchez.apps.dev.k8spin.cloud
    secretName: hello-world-certificate
  rules:
  - host: hello-world.angelbarrerasanchez.apps.dev.k8spin.cloud
    http:
      paths:
      - path: /
        backend:
          serviceName: hello-world
          servicePort: 80
```

### Deploy

```bash
$ pwd
/tmp/hello-world
$ kubectl apply -R -f .
deployment.apps/hello-world created
ingress.extensions/hello-world created
service/hello-world created
```

After a few moments, output should looks like this:

```bash
$ kubectl get deploy,pod,svc,ingress
NAME                                DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
deployment.extensions/hello-world   2         2         2            2           4m27s

NAME                              READY   STATUS    RESTARTS   AGE
pod/hello-world-dbcb5cc9f-2m4lx   1/1     Running   0          4m27s
pod/hello-world-dbcb5cc9f-cqn56   1/1     Running   0          4m26s

NAME                  TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE
service/hello-world   ClusterIP   10.67.251.236   <none>        80/TCP    4m27s

NAME                             HOSTS                                                   ADDRESS         PORTS     AGE
ingress.extensions/hello-world   hello-world.angelbarrerasanchez.apps.dev.k8spin.cloud   35.198.122.28   80, 443   4m27s
```

This example is available here: [*https://hello-world.angelbarrerasanchez.apps.dev.k8spin.cloud*](https://hello-world.angelbarrerasanchez.apps.dev.k8spin.cloud)

[![hello-world](/images/hello-world.png)](https://hello-world.angelbarrerasanchez.apps.dev.k8spin.cloud)
