+++
title = "Use Cases"
chapter = false
weight = 2
pre = "<b>2. </b>"
+++

### Free tier

The free tier offers a limited amount of resources with an unlimited duration. We design it to allow small personal project to being published in an easy way:

- **Static Web Page**: Blogs, documentation, personal sites... You can use tools like: [hugo](https://gohugo.io/), [mkdocs](https://www.mkdocs.org/), [Jekyll](https://jekyllrb.com/) etc... to build a container image and deploy in the platform.
- **Telegram Bot**: You can develop a telegram bot backend, build a container and deploy it in the platform. [More information about telegram bots here](https://core.telegram.org/bots)
- **Little APIs**: If you've got an API [k8spin.cloud](https://k8spin.cloud) is the perfect site to deploy it. We care about publishing your desired url with certificates making it high available.

### Unlocked tier

Once you tried the platform and thinks this is a good way to use kubernetes, you can unlock some use cases:

- **Ephemeral environments on CI/CD systems**: *What if you want to test a change before applying it into the production/live environment?* Read about this topic in the article we published at [medium](https://medium.com/@k8spin/ephemeral-kubernetes-environments-on-ci-cd-systems-3df936a67666). You can create more than one isolated namespace at a time.
- **Stateful application**: You can deploy an application like [sentry.io](https://sentry.io/welcome/) in an unlocked namespace. It needs a relational database to work and persitent volumes are available in the unlocked tier.
- **Medium size project**: We know that not all projects fits into resources provided by free tier. Customize your resources at namespace creation time, you will pay only for your requested resources by minutes.

#### Tell us!

If you think about a use case that you want to share with the comunity, tweet us mentioning [@k8spin.cloud](https://twitter.com/k8spin), write us at [slack](https://slack.k8spin.cloud) or open a merge request in the [repository where lives this site](https://gitlab.com/k8spin-open/documentation).