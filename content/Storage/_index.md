+++
title = "Storage"
chapter = false
weight = 5
pre = "<b>5. </b>"
+++

**Attention:** This feature is beta status.

### Why beta?

Current storage is provided by our cloud provider, Google Cloud. The platform runs on multiple availability zones, Google Cloud persitent volumes do not, the volumes are tied to a specific zone. 

In case of a Google Cloud zone failure, you might not be able to access your data, so act in consequence.

### Backups

All the volumes in the platform are backed up every day *(30 days of retention period)*. In case of a disaster please [contact us](https://slack.k8spin.cloud).
