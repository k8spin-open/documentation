+++
title = "Architecture"
chapter = false
weight = 3
pre = "<b>3. </b>"
+++

### Design principles

Forget about managing *etcd servers, master nodes configuration, internal and external SSL/TLS certificates, load balancers*... *We want to give you quick access to a [kubernetes-based][k] cloud environment*.

### Isolation

It's a *multitenant* design. All users of this product have a space *(tenant)* completely isolated from other users. Every tenant has a limited number of resources, this prevents one user from overusing the system's free resources and even using another user's reserved resources for their applications.

[K8Spin][this] use [Kubernetes][k] mechanisms to prevent one user from affecting another. Some of them are:

- [`ResourceQuota`](https://kubernetes.io/docs/concepts/policy/resource-quotas/)
- [`LimitRange`](https://kubernetes.io/docs/tasks/administer-cluster/manage-resources/memory-default-namespace/#create-a-limitrange-and-a-pod)

Among other mechanisms.

#### Sandbox

Sandboxing is a software management strategy that enforces isolation between software running on a machine, the host operating system, and other software also running on the machine. The purpose is to constrain applications to specific parts of the host’s memory and file-system and not allow it to breakout and affect other parts of the operating system.

![sandbox](/images/sandbox.png)

*Source: [https://cloudplatform.googleblog.com/2018/05/Open-sourcing-gVisor-a-sandboxed-container-runtime.html](https://cloudplatform.googleblog.com/2018/05/Open-sourcing-gVisor-a-sandboxed-container-runtime.html)*

##### gVisor

gVisor intends to solve this problem. It acts as a kernel in between the containerized application and the host kernel. It does this through various mechanisms to support syscall limits, file system proxying, and network access. These mechanisms are a paravirtualization providing a virtual-machine like level of isolation, without the fixed resource cost of each virtual machine.

![sandbox](/images/sandbox.2.png)

*Source: [https://channel9.msdn.com/Blogs/containers/DockerCon-16-Windows-Server-Docker-The-Internals-Behind-Bringing-Docker-Containers-to-Windows](https://channel9.msdn.com/Blogs/containers/DockerCon-16-Windows-Server-Docker-The-Internals-Behind-Bringing-Docker-Containers-to-Windows)*

For your safety and the safety of other users in this platform, all workloads use this sandbox implementation by default *(platform enforces it)*.

##### Limitations

It is not currently possible to use Sandbox along with the following Kubernetes features:

- Accelerators such as GPUs or TPUs
- Istio
- [Portforward](https://kubernetes.io/docs/tasks/access-application-cluster/port-forward-access-application-cluster/)

There is also other cluster scope limitations. Click [here](https://cloud.google.com/kubernetes-engine/docs/how-to/sandbox-pods) to read about it.

### Elasticity

[K8Spin][this] design allows us to grow horizontally in number of users without affecting the performance of any.

### Security

We're building the pillars so you can deploy your application in a cloud [kubernetes][k] environment.

Today we offer security at isolation level *(multitenant)* and protection against abuse of the instances that make it possible for the cluster to run.

### Resilience

All the infrastructure of this project is installed in high availability. From computer resources to storage is ready to be used to deploy applications in high availability.

[K8Spin][this] provides disks to store data. Two storage classes are available in the cluster:

- `standard`: Provides disks belonging to a single availability zone.
- `standard-topology-aware` *(default)*: Provides disks belonging to a single availability zone. Useful in stateful set applications. More information in the [kubernetes blog](https://kubernetes.io/blog/2018/10/11/topology-aware-volume-provisioning-in-kubernetes/).

You can find more information [about the storage in its own section](/storage).

[k]: https://kubernetes.io/
[this]: https://k8spin.cloud
