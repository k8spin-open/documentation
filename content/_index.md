# Overview of K8Spin

### [K8spin](https://k8spin.cloud) is a multitenant [Kubernetes][k] namespace as a Service solution.
A namespace is an isolated and secure piece of a kubernetes cluster where you will be able to deploy your applications.

![K8Spin Namespaces](/images/k8spin_namespaces.svg)

#### Ready to start?
1. Register at [K8spin](https://k8spin.cloud)
2. Follow our [getting started guide](getting-started)
[k]: https://kubernetes.io/