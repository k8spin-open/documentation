FROM registry.gitlab.com/k8spin-open/container-images/hugo:0.55.6

RUN mkdir /site

WORKDIR /site

ADD . /site

USER 1001

CMD hugo serve --bind 0.0.0.0