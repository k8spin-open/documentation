# K8spin documentation

## How to edit this

First, you have to have installed [`hugo`](https://gohugo.io). At the moment of this writting the latest hugo version is: [0.55.6](https://github.com/gohugoio/hugo/releases/tag/v0.55.6).

The you should clone this repository locally including its submodules:

```bash
$ git clone --recurse-submodules git@gitlab.com:k8spin/documentation.git
Cloning into 'documentation'...
remote: Enumerating objects: 421, done.
remote: Counting objects: 100% (421/421), done.
remote: Compressing objects: 100% (247/247), done.
remote: Total 421 (delta 150), reused 386 (delta 130)
Receiving objects: 100% (421/421), 3.22 MiB | 1.61 MiB/s, done.
Resolving deltas: 100% (150/150), done.
Submodule 'themes/learn' (https://github.com/matcornic/hugo-theme-learn) registered for path 'themes/learn'
Cloning into '/tmp/documentation/themes/learn'...
remote: Enumerating objects: 2099, done.
remote: Total 2099 (delta 0), reused 0 (delta 0), pack-reused 2099
Receiving objects: 100% (2099/2099), 13.33 MiB | 912.00 KiB/s, done.
Resolving deltas: 100% (1136/1136), done.
Submodule path 'themes/learn': checked out '7f5e927f0c5c782fc8657951ead095e8adf86fe3'
```

Then you are ready to server the content editing it locally:

```bash
$ cd documentation
$ hugo serve

                   | EN
+------------------+----+
  Pages            | 24
  Paginator pages  |  0
  Non-page files   |  0
  Static files     | 85
  Processed images |  0
  Aliases          |  0
  Sitemaps         |  1
  Cleaned          |  0

Total in 48 ms
Watching for changes in /tmp/documentation/{content,data,layouts,static,themes}
Watching for config changes in /tmp/documentation/config.toml
Environment: "development"
Serving pages from memory
Running in Fast Render Mode. For full rebuilds on change: hugo server --disableFastRender
Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)
Press Ctrl+C to stop
```

Open your browser and navigate to: [http://localhost:1313/](http://localhost:1313/).
Now you can edit any documentation section and hugo will live reload changes in your browser.

## Theme

We are using an awesome hugo theme named: [learn](https://learn.netlify.com/en/)

Take a look to the documentation about this theme.

### Snippets

If you want to create a new menu entry you can add it manually or use hugo new command generator:

```bash
$ hugo new --kind chapter <name>/_index.md
```

## Serveo

If you want to permit access locally to a college (hi Pau):

```bash
$ ssh -R k8spindocs:80:localhost:1313 serveo.net
```

Then the available url will be: [https://k8spindocs.serveo.net/](https://k8spindocs.serveo.net/)

### Links

- https://cloud.google.com/storage/docs/hosting-static-website
- https://cloud.google.com/storage/docs/domain-name-verification
- https://cloud.google.com/storage/docs/access-control/making-data-public#buckets
- https://cloud.google.com/storage/docs/gsutil/commands/rsync